This folder contains the scripts to perform genus synthesis.

You will have to provide a user input tcl file similar to the file provided in
scripts/example_user_input.tcl.

You will also have to provide a pd_tcl file similar to the file provided in 
scripts/example_pd_input.tcl

To perform synthesis:

  .. code-block:: bash
      
      cd run
      make synth user_tcl=<path to your user input tcl> pd_tcl=<path to pd input tcl without the
      .tcl extension>

To clean the folder:

  .. code-block:: bash
      
      cd run
      make clean


