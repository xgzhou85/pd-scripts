set report_default_significant_digits 3 ;
set sh_source_uses_search_path true ;
set search_path  ;

source ../period.tcl

if {![file exists rpt/pt]} { file mkdir rpt/pt }

set search_path "/scratch/libs/tsmc_65nm/" 

#set link_path "* tcbn65lpbwp12twc_ccs.db tcbn65lpbwp12tlvtwc_ccs.db" ;
set link_path "* tcbn65lpbwp12twc_ccs.db " ;

read_verilog ../run/out/${top}_final.v
link_design -verbose ${top}
current_design ${top}


if {![info exists clkf_Mhz]} {set clkf_Mhz 500} ;     # Setting default frequency to 500MHz
if {![info exists max_uncert]} {set max_uncert 200} ; # Setting default uncertainty to 200ps

  set clk_period [expr 1000.0/${freq_mhz}]
  set clkp ${clk_period} 
  set clku ${max_uncert}

create_clock -name clk         -p $clkp [get_ports CLK]


set_clock_uncertainty ${clku} -setup [all_clocks]

set_input_delay   [expr (0.3*$clkp)]      -clock clk [all_inputs ];
set_output_delay  [expr (0.3*$clkp)]      -clock clk [all_outputs];

reset_timing_derate
set_timing_derate -late -data 1.10 ;        # Setting derate to 10%
set_load         -pin_load 0.00806 [all_outputs]
set_max_transition 0.55 [current_design]

group_path -name f2f -from [all_registers]  -to [all_registers]
group_path -name i2f -from [all_inputs ]  -to [all_registers]
group_path -name f2o -from [all_registers]  -to [all_outputs]
group_path -name i2o -from [all_inputs ]  -to [all_outputs]
  
set_wire_load_mode top
set_wire_load_model -name TSMC8K_Lowk_Conservative


set report_default_significant_digits 4

set timing_remove_clock_reconvergence_pessimism true
set timing_clock_reconvergence_pessimism same_transition
####Use this only for zero wire-load STA###################
##source ./wire_load.tcl
#############################################################
#
update_timing -full
#
report_timing -derate -unique_pins -nosplit -max_paths 50 -input -nets -cap -tran -nosplit -sig 3                 > rpt/pt/$top.setup.all.rpt.by_group
report_timing -derate -unique_pins -nosplit -max_paths 50 -input -nets -cap -tran -nosplit -sig 3 -sort_by slack  > rpt/pt/$top.setup.all.rpt.by_slack
report_timing -derate -unique_pins -nosplit -max_paths 20 -input -nets -cap -tran -nosplit -sig 3 -group f2f      > rpt/pt/$top.setup.f2f.rpt
report_timing -derate -unique_pins -nosplit -max_paths 20 -input -nets -cap -tran -nosplit -sig 3 -group i2f      > rpt/pt/$top.setup.i2f.rpt
report_timing -derate -unique_pins -nosplit -max_paths 20 -input -nets -cap -tran -nosplit -sig 3 -group f2o      > rpt/pt/$top.setup.f2o.rpt
report_timing -derate -unique_pins -nosplit -max_paths 20 -input -nets -cap -tran -nosplit -sig 3 -group i2o      > rpt/pt/$top.setup.i2o.rpt
report_qor > rpt/pt/$top.qor

exit
